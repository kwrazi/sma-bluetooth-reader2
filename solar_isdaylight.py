#!/usr/bin/env python2
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

import sys
import solar_almanac
import datetime

if len(sys.argv)>1 and sys.argv[1].isdigit():
    timestamp = datetime.datetime.fromtimestamp(float(sys.argv[1]))
else:
    sys.exit()

almanac = solar_almanac.solar_almanac()
rising = almanac.local_mean_time('rising')
setting = almanac.local_mean_time('setting')
#print('sunrise: %f' % (rising))
#print('sunset:  %f' % (setting))
hourofday = timestamp.hour + timestamp.minute / 60.0 + timestamp.second / 3600.0
#print('timestamp:  %f' % (hourofday))


if (hourofday > rising) and (hourofday < setting):
    print('1')
else:
    print('0')
