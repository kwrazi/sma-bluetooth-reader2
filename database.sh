#!/usr/bin/env bash
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

CONFIG="sma_config.sh"
CMD="./sma_sqlite.exe"

if ! hcitool dev | grep -q hci0; then
#   echo "rebooting bluetooth device"
   hciconfig hci0 down
   hciconfig hci0 up
#else
#   echo "bluetooth device found."
fi

if [ -f "${CONFIG}" ]; then
    . ${CONFIG}
    if [ -x "${CMD}" ]; then
        logger -p local3.info "Pulling data from SMA inverter..."
        exec ${CMD} \
            --MAC "${MACADDR}" \
            --password "${PASSWORD}" --5minute --daily \
            --sqlite "${SQLFILE}"
    else
        echo "Error ${CMD} not found."
    fi
else
    cat <<EOF
Breakage:
  ${CONFIG} file not found.
Fix:
  Copy sma_config.example.sh to ${CONFIG}. Edit the parameters
EOF
fi
