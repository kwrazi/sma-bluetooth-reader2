#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

# protect this files if you can with the appropriate permission
### inverter
PASSWORD="not_telling"
MACADDR="00:00:00:00:00:00"

### sqlite
SQLFILE="data.db"
SQLTABLE="yield_5m"
SQLCOL1="timestamp"
SQLCOL2="energy"

### rrdtool
RRDFILE="sma.rrd"
RRDSTEP=300        # step time
RRDNAME="energy"   # name
RRDHB=600          # heart beat
RRDTYPE="COUNTER"  # data type
RRDMAX=6000        # max energy

### cron
CRONPATH="/etc/cron.d/"
CRONUSER="${USER}"
INSTALLDIR="/opt/git/sma-bluetooth-reader2"
WEBDIR="/var/www/html/sunnyboy/"
