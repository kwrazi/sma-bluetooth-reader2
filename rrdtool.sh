#!/usr/bin/env bash
#

CONFIG="sma_config.sh"
if [ -f "${CONFIG}" ]; then
    . ${CONFIG}
fi

# Get tables
#sqlite3 data.db ".tables"
# Get column names
#sqlite3 data.db "pragma table_info(yield_5m)"
# Get first row
#sqlite3 data.db "select * from yield_5m order by rowid asc limit 1"
# Get last row
#sqlite3 data.db "select * from yield_5m order by rowid desc limit 1"
# Get all data
#sqlite3 data.db "select * from yield_5m"

function get_first_timestamp () {
    FILENAME="$1"
    TIME=0
    if [ -f "${FILENAME}" ]; then 
        sqlite3 "${FILENAME}" "select * from yield_5m order by rowid asc limit 1" | cut -d\| -f1
    else
        echo 0
        return 1
    fi
    return 0
}

function get_last_rrdtime () {
    if [ -f "${RRDFILE}" ]; then
        rrdtool last "${RRDFILE}" | cut -d: -f1
    else
        echo 0
        return 1
    fi
    return 0
}

function create_rrd () {
    START="$1"
    rrdtool create ${RRDFILE} \
        --start ${START} \
        --step ${RRDSTEP} \
        DS:${RRDNAME}:${RRDTYPE}:${RRDHB}:0:${RRDMAX} \
        DS:daylight:GAUGE:300:0:10 \
        RRA:AVERAGE:0.5:1:2016 \
        RRA:AVERAGE:0.5:3:2016 \
        RRA:AVERAGE:0.5:12:672 \
        RRA:AVERAGE:0.5:288:1461 \
        RRA:AVERAGE:0.5:2016:835 \
        RRA:MIN:0.5:3:2016 \
        RRA:MIN:0.5:12:672 \
        RRA:MIN:0.5:288:1461 \
        RRA:MIN:0.5:2016:835 \
        RRA:MAX:0.5:3:2016 \
        RRA:MAX:0.5:12:672 \
        RRA:MAX:0.5:288:1461 \
        RRA:MAX:0.5:2016:835
}

function sqldb_import_rrd () {
    # fill historical value
    LAST=`get_last_rrdtime`
    LASTSTR=`date -d@"${LAST}"`
    echo "Adding data from ${LASTSTR}..."
    for i in `sqlite3 ${SQLFILE} "SELECT * FROM ${SQLTABLE} WHERE ${SQLCOL1} > ${LAST}"`; do
        TS=`echo "$i" | cut -d\| -f1`
        DL=`./solar_isdaylight.py ${TS}`
        ENERGY=`echo "$i" | cut -d\| -f2`
        DATESTR=`date -d@"${TS}"`
        DATA=`echo "$i" | tr '|' ':'`:${DL}
        echo "  Adding ${DATESTR} : ${DATA}"
        rrdtool update "${RRDFILE}" ${DATA}
    done
}

function create_graph () {
    OUTFILE="$1"
    START="$2"
    END="$3"
    MAX=5000

    now_sec=`date -d "now" +%s`
    rrdtool graph "${OUTFILE}" --start "${START}" --end "${END}" \
        --width ${RRDXRES} --height ${RRDYRES} --step ${RRDSTEP} \
        --lower-limit 0 --title "Energy output (kWh)" \
        --vertical-label "Output in kWh" \
        "DEF:Power=${RRDFILE}:energy:AVERAGE" \
        "DEF:daylight=${RRDFILE}:daylight:AVERAGE" \
        "CDEF:dark=daylight,0,EQ,${MAX},daylight,IF" \
        "CDEF:kWh=Power,3600,*" \
        "VDEF:Avg=kWh,AVERAGE" \
        "VDEF:Min=kWh,MINIMUM" \
        "VDEF:Max=kWh,MAXIMUM" \
        "VDEF:Last=kWh,LAST" \
        "VDEF:Total=Power,TOTAL" \
        AREA:dark#808080:"Night\t" \
        LINE2:Avg#008000:"Average Power\t" \
        AREA:kWh#0000FF:"Solar Energy\t" \
        HRULE:${MAX}#FF0000:"Inverter Limit\l" \
        VRULE:${now_sec}#FF0000 \
        GPRINT:Min:"Minimum\: %6.2lf%sWh\t" \
        GPRINT:Max:"Maximum\: %6.2lf%sWh\t" \
        GPRINT:Avg:"Average\: %6.2lf%sW\l" \
        GPRINT:Last:"Last\:    %6.2lf%sWh\t" \
        GPRINT:Total:"Total\:   %6.2lf%sWh\l"
    if [ -f ${OUTFILE} ]; then
        NEWGRAPH="${OUTFILE} ${NEWGRAPH}"
    fi
}

START=`get_first_timestamp data.db`
if [ ${START} -gt 0 ]; then
    if [ ! -f "${RRDFILE}" ]; then
        create_rrd "${START}"
    fi
fi

if [ -f "${RRDFILE}" ]; then
    # fill historical value
    sqldb_import_rrd

    NEWGRAPH=""
    # generate graphs - 1 day
    create_graph "${RRDPNG}-today.png" "0:00"     "start+1d"
    create_graph "${RRDPNG}-yday.png"  "0:00-1d"  "start+1d"
    create_graph "${RRDPNG}-week.png"  "end-1w"   "23:59"
    create_graph "${RRDPNG}-month.png" "end-1m"   "23:59"
    create_graph "${RRDPNG}-year.png"  "end-1y"   "23:59"
fi

### for debugging
#for f in ${NEWGRAPH} ; do
#    if [ -f "$f" ]; then
#        display "$f" &
#    fi
#done
