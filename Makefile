# 
# Kiet To <kwrazi@gmail.com>
# December 2013
#

#---------------------------------------------------------------------
# executables
#---------------------------------------------------------------------
     SHELL := /bin/bash

        CC := g++
  CPPCHECK := cppcheck
        MD := mkdir -p
        RM := rm

  MAKEDEPS := make.deps
#---------------------------------------------------------------------
# variables
#---------------------------------------------------------------------
        OPT = -O3 -g -std=c++11

     INCDIR = -I. $(EXTRA_INCDIR)
     LIBDIR = -L. $(EXTRA_LIBDIR)

       LIBS = -lbluetooth -lsqlite3 -lcurl $(EXTRA_LIBS)

     CFLAGS = $(OPT)
    LDFLAGS = $(LIBDIR) $(LIBS)

#---------------------------------------------------------------------
# rules
#---------------------------------------------------------------------
all: 
	make -C src
	mv src/*.exe .

clean:
	make -C src clean
	$(RM) -f *.png *~ *.pyc *.exe
