<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="js/vendor/modernizr-2.7.1.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    
	<div id="accessibility">
	  <a href="#nav">Skip to Navigation</a> |
	  <a href="#main-content">Skip to Content</a>
	</div>

    <div id="container">
	  <header role="banner">
        
		<div id="logo"> 
		  <a rel="home" href="/" title="Home">...site title or logo...</a> 
		</div>
		
		<nav id="primary-nav">
		  <ul>
			<li><a href="#">Nav 1</a></li>      
			<li><a href="#">Nav 2</a></li>
			<li><a href="#">Nav 3</a></li>
		  </ul>
		</nav>
	  </header><!--end header-->
      
	  <div id="main-content" role="main">

		<h1>Page Title</h1>

	<p>...Main Page Content...</p>

	<div id="slideshow">
	  <ul class="slider">
	    <li>
          <input type="radio" id="slide5" name="slide">
          <label for="slide5"></label>
          <img src="img/sma-year.png" alt="year" />
        </li>
	    <li>
          <input type="radio" id="slide4" name="slide">
          <label for="slide4"></label>
          <img src="img/sma-month.png" alt="month" /></li>
	    <li>
          <input type="radio" id="slide3" name="slide">
          <label for="slide3"></label>
          <img src="img/sma-week.png" alt="week" />
        </li>
	    <li>
          <input type="radio" id="slide2" name="slide">
          <label for="slide2"></label>
          <img src="img/sma-yday.png" alt="yesterday" />
        </li>
	    <li>
          <input type="radio" id="slide1" name="slide" checked>
          <label for="slide1"></label>
          <img src="img/sma-today.png" alt="today" />
        </li>
	  </ul>
	</div>

    <div id="daily_table">
      <?php
$filename = "daily.xml";
$handle = fopen($filename, "r");
if ($handle) 
{
   while (($line = fgets($handle)) != false) {
      // process the line read.
      print "$line";
   }
   fclose($handle);
} 
else 
{
   // error opening the file.
   print "Daily data file is missing.";
}
         ?>
    </div>

      </div> <!--end main div-->

      <div id="secondary-content">
	...secondary content/sidebar goes here...
      </div><!--end secondary div-->

      <aside id="sidebar">
	....site sidebar here....7
      </aside>

      <footer id="page-footer">
	<p>© Copyright Your Name 2011.</p>
      </footer>

	</div> <!--! end of #container -->
    
    <!-- <p>Hello world! This is HTML5 Boilerplate.</p> -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
      <script src="js/plugins.js"></script>
      <script src="js/main.js"></script>

      <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
      <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
      </script>
  </body>
</html>
