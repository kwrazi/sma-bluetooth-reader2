#!/usr/bin/env python2
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#
# Algorithm pulled from:
#   http://williams.best.vwh.net/sunrise_sunset_algorithm.htm

import math
import datetime
import time
from dateutil.tz import tzlocal

class solar_almanac:

    ZENITY_ANGLE = {
        'official' : (90 + 50/60),
        'civil'   : 96,
        'nautical' : 102,
        'astronomical' : 108 }

    def __init__(self,dt = None):
        if dt is None:
            self.date = datetime.datetime.now(tzlocal())
        else:
            self.date = dt.replace(tz=tzlocal())
        self.latitude = -34.9290
        self.longitude = 138.6010
        self.zenith = self.ZENITY_ANGLE['civil']
    
    # ----
    def _sin(self,value):
        return math.sin(math.radians(value))

    def _cos(self,value):
        return math.cos(math.radians(value))

    def _tan(self,value):
        return math.tan(math.radians(value))

    def _asin(self,value):
        return math.degrees(math.asin(value))

    def _acos(self,value):
        return math.degrees(math.acos(value))

    def _atan(self,value):
        return math.degrees(math.atan(value))

    # ----
    def utc_offset(self,day = None,month = None,year = None):
        if day is not None:
            self.date = self.date.replace(day=day)
        if month is not None:
            self.date = self.date.replace(month=month)
        if year is not None:
            self.date = self.date.replace(year=year)
        offset = self.date.utcoffset().seconds / 3600.0
        self.utcoffset = offset
        return offset

    # ----
    def zenith(self,ztype):
        if self.ZENITY_ANGLE.has_key(ztype):
            self.zenith = ZENITY_ANGLE[ztype]
        else:
            raise KeyError('Invalid zenith key')
        return self.zenith

    def day_of_the_year(self,day = None,month = None,year = None):
        if day is None:
            day = self.date.day
        else:
            self.date = self.date.replace(day=day)

        if month is None:
            month = self.date.month
        else:
            self.date = self.date.replace(month=month)

        if year is None:
            year = self.date.year
        else:
            self.date = self.date.replace(year=year)

        N1 = math.floor(275 * month / 9)
        N2 = math.floor((month + 9) / 12)
        N3 = (1 + math.floor((year - 4 * math.floor(year / 4) + 2) / 3))
        return N1 - (N2 * N3) + day - 30

    def longitude_time(self):
        tm = dict()
        lngHour = self.longitude / 15
        tm['rising'] = self.day_of_the_year() + ((6 - lngHour)) / 24
        tm['setting'] = self.day_of_the_year() + ((18 - lngHour)) / 24
        self.tm = tm
        return tm

    def mean_anomaly(self):
        self.M = dict()
        for (k,tm) in self.longitude_time().items():
            self.M[k] = (0.9856 * tm) - 3.289
        return self.M

    def true_longitude(self):
        self.true_long = dict()
        for (k,M) in self.mean_anomaly().items():
            self.true_long[k] = (M + (1.916 * self._sin(M)) + \
                                 (0.020 * self._sin(2 * M)) + 282.634) % 360
        return self.true_long

    def right_ascension(self):
        RA = dict()
        for (k,L) in self.true_longitude().items():
            RA[k] = self._atan(0.91764 * self._tan(L))
            RA[k] = RA[k] % 360
            Lquadrant  = math.floor(L/90) * 90
            RAquadrant = math.floor(RA[k]/90) * 90
            RA[k] = RA[k] + (Lquadrant - RAquadrant)
            RA[k] = RA[k] / 15
        self.RA = RA
        return RA
    
    def solar_sinDeclination(self):
        self.sinDec = dict()
        for (k,L) in self.true_longitude().items():
            self.sinDec[k] = 0.39782 * self._sin(L)
        return self.sinDec

    def solar_cosDeclination(self):
        self.cosDec = dict()
        for (k,sinDec) in self.solar_sinDeclination().items():
            self.cosDec[k] = self._cos(self._asin(sinDec))
        return self.cosDec

    def hour_angle(self):
        cosH = dict()
        H = dict()
        for (k,cosDec) in self.solar_cosDeclination().items():
            sinDec = self.sinDec[k]
            cosH[k] = (self._cos(self.zenith) - (sinDec * self._sin(self.latitude))) / (cosDec * self._cos(self.latitude))
            if cosH[k] > 1:
                raise ValueError('the sun never rises on this location (on the specified date)')
            if cosH[k] < -1:
                raise ValueError('the sun never sets on this location (on the specified date)')
            if k is 'rising':
                H[k] = 360 - self._acos(cosH[k])
            if k is 'setting':
                H[k] = self._acos(cosH[k])
            H[k] = H[k] / 15
            
        self.H = H
        return H

    def local_mean_time(self,state):
        self.hour_angle()
        self.right_ascension()
        if state in self.H:
            T = self.H[state] + self.RA[state] - (0.06571 * self.tm[state]) - 6.622
            UT = (T - (self.longitude / 15)) % 24
            localT = (UT + self.utc_offset()) % 24
        else:
            raise KeyError('state not "rising" or "setting".')
        return localT

if __name__ == '__main__':
    almanac = solar_almanac()
    # almanac.latitude =  -33.8678500  # sydney
    # almanac.longitude = 151.2073200  # sydney
    print(almanac.day_of_the_year())
    # print(almanac.day_of_the_year(12,6,2013))  # summer/winter season
    # print(almanac.day_of_the_year(12,12,2013)) # winter/summer season
    print(almanac.longitude_time())
    print(almanac.mean_anomaly())
    print(almanac.true_longitude())
    print(almanac.right_ascension())
    print(almanac.solar_sinDeclination())
    print(almanac.solar_cosDeclination())
    print(almanac.hour_angle())
    print(almanac.utc_offset())
    # print(almanac.utc_offset(12,6,2013))   # summer/winter season
    # print(almanac.utc_offset(12,12,2013))  # winter/summer season
    rising = almanac.local_mean_time('rising')
    setting = almanac.local_mean_time('setting')
    print('sunrise: %f' % (rising))
    print('sunset:  %f' % (setting))
    print('sunrise: %s' % str(datetime.timedelta(seconds=rising*3600)))
    print('sunset:  %s' % str(datetime.timedelta(seconds=setting*3600)))
