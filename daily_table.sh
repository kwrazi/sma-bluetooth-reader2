#!/usr/bin/env bash
#
#
#

RRDFILE="sma.rrd"

function print_total() {
    TIME=$1

    rrdtool graph daily.png \
        DEF:Power=${RRDFILE}:energy:AVERAGE:start="${TIME}":end='start+24h' \
        CDEF:kWday=Power,86400,\* \
        VDEF:Total=kWday,AVERAGE \
        PRINT:Total:"%6.2lf %sWh" | grep -v 0x0
}

function print_max_energy() {
    TIME=$1

    rrdtool graph daily.png \
        DEF:Power=${RRDFILE}:energy:AVERAGE:start="${TIME}":end='start+24h' \
        CDEF:kWh=Power,3600,\* \
        VDEF:Max=kWh,MAXIMUM \
        PRINT:Max:"%6.2lf %sWh" \
        PRINT:Max:" @ %H\:%M":strftime | grep -v 0x0
}

function print_ave_power() {
    TIME=$1

    rrdtool graph daily.png \
        DEF:Power=${RRDFILE}:energy:AVERAGE:start=${TIME}:end='start+24h' \
        CDEF:kWh=Power,3600,\* \
        VDEF:AvePower=kWh,AVERAGE \
        PRINT:AvePower:"%6.2lf %sW" | grep -v 0x0
}

TODAY=$(date +%Y-%m-%d -d now)

DAY="${TODAY}"
cat <<EOF
<table class="energy_table">
<thead>
<tr>
 <th>Date</th>
 <th>Total Daily Energy</th>
 <th>Peak Energy</th>
 <th>Average Power</th>
</tr>
</thead>
<tbody>
EOF
for i in `seq 1000`; do
    START=$(date -d "${DAY} 0:00:00")
    TIME=$(date +%s -d "${START}")
    # echo "${DAY} -- ${START} -- ${TIME}"
    TOTAL=$(print_total ${TIME})
    if echo ${TOTAL} | grep -q nan; then
        break
    else
        echo "<!-- ${DAY} => ${START} = ${TIME} -->"
        PEAK=$(print_max_energy ${TIME})
        AVEPOWER=$(print_ave_power ${TIME})

        DAYSTR=$(date +"%Y-%m-%d %a" -d "${DAY}")
        DATEISO=$(date +"%Y%m%d" -d "${DAY}")
        echo "<tr id=\"${DATEISO}\">"
        echo " <td class=\"date\">$DAYSTR</td>"
        echo " <td class=\"energy\">${TOTAL}</td>"
        echo " <td class=\"peak\">${PEAK}</td>"
        echo " <td class=\"avepower\">${AVEPOWER}</td></tr>"
    fi
    # get previous day's date
    DAY=$(date +%Y-%m-%d -d "${DAY} - 1 day")
done
cat <<EOF
</tbody>
</table>
EOF

