#!/usr/bin/env bash
# 
# Kiet To <kwrazi@gmail.com>
# December 2013
#

CONFIG="sma_config.sh"
if [ -f "${CONFIG}" ]; then
    echo "Reading configuration..."
    . ${CONFIG}
else
    cat <<EOF
Breakage:
  ${CONFIG} file not found.
Fix:
  Copy sma_config.example.sh to ${CONFIG}. Edit the parameters
EOF
    exit
fi

if [ -d "${CRONPATH}" ]; then
    DST=/etc/cron.d/sma-update.sh
else
    DST=sma-update.sh
    echo "Cront path '${CRONPATH}' not found. You will need to add manually."
fi

if [ -d "${INSTALLDIR}" ]; then 
    echo "Installing cron file: ${DST}..."
    echo "# cron job for sma-bluetooth-reader2" > ${DST}
    # Check every 5mins except on the hour
    echo "1-59/5 * * * * ${CRONUSER} cd ${INSTALLDIR} && ./database.sh" >> ${DST} 
    WEBIMGDIR=${WEBDIR}/img
    if [ -d "${WEBIMGDIR}" ]; then
        # Generate image every hour
        echo "0 * * * * ${CRONUSER} cd ${INSTALLDIR} && ./database.sh && ./rrdtool.sh && mv ${INSTALLDIR}/*.png ${WEBIMGDIR}" >> ${DST}
        # Update daily total table at end of day
        echo "59 23 * * * ${CRONUSER} cd ${INSTALLDIR} && ./daily_table.sh > ${WEBDIR}/daily.xml" >> ${DST}
    else
        echo "Web path '${WEBIMGDIR}' not found..."
    fi
else
    echo "Path '${INSTALLDIR}' not found..."
fi
